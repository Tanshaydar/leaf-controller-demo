/*
 * Fumera Ar-Ge Yazılım Müh. İml. San. ve Tic. Ltd. Şti. | Copyright 2012-2013
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright 
 * notice, this list of conditions, and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions, and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution. 
 * 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR 
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */

package fumera.controllers;

import fumera.models.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Tansel
 */
public class JavaDBtoObj {
    
    public static void inserYeniUserToDB( User user){
        Connection connection = JavaConnector.ConnectDB();
        PreparedStatement statement = null;
        
        try {
            statement = connection.prepareStatement("INSERT INTO " + JavaConnector.DBname() + ".users "
                    + "(user_id, userRealName, username, password, level, firma) VALUES ( ?, ?, ?, ?, ?, ?)");
            statement.setNull( 1, java.sql.Types.INTEGER);
            statement.setString( 2, user.getUserRealName());
            statement.setString( 3, user.getUserName());
            statement.setString( 4, user.getUserPass());
            statement.setString( 5, user.getUserLevel());
            System.out.println("Level: " + user.getUserLevelInt());
            statement.setInt( 6, 0);
            statement.executeUpdate();
       } catch (SQLException e) {
           FileLogger.errorLogger(e.toString());
           JOptionPane.showMessageDialog(null, e);
        } finally{
            try {
                statement.close();
            } catch (SQLException e) {
                FileLogger.errorLogger(e.toString());
            }
        }
    }
    public static void updateUserToDB( User user){
        Connection connection = JavaConnector.ConnectDB();
        PreparedStatement statement = null;
        
        try {
            statement = connection.prepareStatement("UPDATE " + JavaConnector.DBname() + ".users"
                    + " SET userRealName=?, username=?, password=?, level=? WHERE users.user_id=?;");
            statement.setString( 1, user.getUserRealName());
            statement.setString( 2, user.getUserName());
            statement.setString( 3, user.getUserPass());
            statement.setInt(4, user.getUserLevelInt());
            System.out.println("Level: " + user.getUserLevelInt());
            statement.setInt( 5, user.getUserID());
            statement.executeUpdate();
        } catch (SQLException e) {
            FileLogger.errorLogger(e.toString());
           JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                FileLogger.errorLogger(e.toString());
            }
        }
    }
    public static void UserSil( int userID){
        Connection connection = JavaConnector.ConnectDB();
        PreparedStatement statement = null;
        
        try {
            statement = connection.prepareStatement( "DELETE FROM " + JavaConnector.DBname() + ".users WHERE users.user_id=?;");
            statement.setInt(1, userID);
            statement.executeUpdate();
        } catch (SQLException e) {
            FileLogger.errorLogger(e.toString());
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                FileLogger.errorLogger(e.toString());
            }
        }
    }
    
    public static ArrayList<User> getUsersFromDB(){
        ArrayList<User> users = new ArrayList<User>();
        
        Connection connection = JavaConnector.ConnectDB();
        PreparedStatement statement = null;
        ResultSet result = null;
        
        try {
            statement = connection.prepareStatement("SELECT * FROM " + JavaConnector.DBname() + ".users");
            result = statement.executeQuery();
            while( result.next()){
                users.add( new User( result.getInt("user_id"), result.getString("userRealName"), result.getString("username"),
                        result.getString("password"), result.getString("level"), result.getInt("firma")));
            }
        } catch (SQLException e) {
            FileLogger.errorLogger(e.toString());
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                FileLogger.errorLogger(e.toString());
            }
        }
                
        return users;
    }
}
