package fumera.test;


import com.serotonin.io.serial.SerialParameters;
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusLocator;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.code.RegisterRange;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.msg.ModbusRequest;
import com.serotonin.modbus4j.msg.ReadCoilsRequest;
import com.serotonin.modbus4j.msg.ReadResponse;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tansel
 */
public class MasterTest {
    
    public static void main(String args[]){
        
        int slaveID = 1;
        int startOffset = 20;
        int numberOfBits = 25;
        

        ModbusFactory factory = new ModbusFactory();

        SerialParameters params = new SerialParameters();
        params.setCommPortId("COM5");
        params.setBaudRate(9600);
        params.setDataBits(7);
        params.setParity(2);
        params.setStopBits(1);

        //ModbusMaster master = factory.createRtuMaster(params);
        ModbusMaster master = factory.createAsciiMaster(params);
        master.setTimeout( 500);
        master.setRetries( 4);
        System.out.println("Connection parameters are set...");

        try {
            master.init();
            System.out.println("Modbus Initialized...");
            ///////////////////////

        } catch (ModbusInitException e) {
            System.out.println(e.toString());
        }
        
        try {
            
            //System.out.println("Data: " + master.getValue( 1, RegisterRange.INPUT_STATUS, 46, DataType.BINARY));
            /*
            for( int i = 1280; i < 4100; i++) {
                ModbusLocator locator = new ModbusLocator(1, RegisterRange.COIL_STATUS, i, DataType.BINARY );
                master.setValue(locator, true);
                System.out.println(i +") COIL: " + master.getValue(locator));
            }*/
            
            /*
            ModbusLocator locator = new ModbusLocator(1, RegisterRange.COIL_STATUS, 1282, DataType.BINARY );
            master.setValue(locator, true);
            System.out.println("COIL: " + master.getValue(locator));*/
            for( int i = 1024; i < 1034; i++) {
                ModbusLocator locator = new ModbusLocator(1, RegisterRange.INPUT_STATUS, i, DataType.BINARY );
                //master.setValue(locator, true);
                System.out.println(i+" INPUT STATUS: " + master.getValue(locator));
            }
            
            //for( int i = 1; i < 443000; i++)
            //System.out.println( "Data: " + master.getValue( 1, 1025, DataType.BINARY));
            
        } catch (ModbusTransportException | ErrorResponseException e) {
            System.out.println(e.toString());
        }
        finally{
            master.destroy();
            System.out.println("Master Destroyed...");
        }

    }
    
}
